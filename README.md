MCP for flight simulator X-Plane 10/11

Consists of two parts:

- Arduino based hardware of MCP and EFIS;

- plugin for the simulator side;

Folders:
  
  arduino_mcp - firmware for Arduino Mega2560 which MCP is based on
  
  doc - some text and graphics information about hardware and software
  
  product - plugin for flight simulator
  
  xplane_io_plugin  - C source code of plugin
  

