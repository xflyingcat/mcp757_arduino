/*--------------------------------------------------------
  "THE BEER-WARE LICENSE" (Revision 42):
  Alex Kostyuk wrote this code. As long as you retain this
  notice you can do whatever you want with this stuff.
  If we meet some day, and you think this stuff is worth it,
  you can buy me a beer in return.
----------------------------------------------------------*/

#include "includes.h"


char *get_fmt(int len)
{
static char fmt_buf[5];

  fmt_buf[0] = '\%';
  fmt_buf[1] = '0'+len;
  fmt_buf[2] = 'd';
  fmt_buf[3] = 0x00;

return fmt_buf;
}

char *get_fmt0(int len)
{
static char fmt_buf[6];

  fmt_buf[0] = '\%';
  fmt_buf[1] = '0';
  fmt_buf[2] = '0'+len;
  fmt_buf[3] = 'd';
  fmt_buf[4] = 0x00;

return fmt_buf;
}




void display_update(SESSION_WORK_SPACE *sws)
{
char buf[1+3+128+1];
int i,key_code;
char tmp_buf[16];

   memset(buf,' ',133);
   for(i=0;i<MAX_KEYS;i++)
   {
    if(!key_config[i].mode)
    {
      break;
    }
    if((key_config[i].mode == MODE_POLL) && (key_config[i].port == sws->port_id))
    {
      key_code = key_config[i].key_id;
      if(key_config[i].len) //  not zero length of data means numeric value
      {
       if(key_config[i].opt & 1)
          sprintf(tmp_buf,get_fmt0(key_config[i].len),output_data[key_code]);
       else
          sprintf(tmp_buf,get_fmt(key_config[i].len),output_data[key_code]);

       memcpy(&buf[4+8*(15-key_config[i].devadr)+key_config[i].pos],
              tmp_buf,
              key_config[i].len);
      }

    }
   }
      buf[0] = '{';
      buf[1] = '7';
      buf[2] = 'S';
      buf[3] = 'G';
      buf[132] = '}';
      serial_write(sws,(unsigned char*)buf,133);
      //log_write(buf);
}

void lamps_update(SESSION_WORK_SPACE *sws)
{
char buf[1+3+64+1];
int i,key_code;

   memset(buf,' ',69);

   for(i=0;i<MAX_KEYS;i++)
   {
    if(!key_config[i].mode)
    {
      break;
    }
    if(key_config[i].mode == MODE_POLL && key_config[i].port == sws->port_id)
    {
      key_code = key_config[i].key_id;
      if(key_config[i].len == 0) // zero length of data means that is a signal lamp state
      {
        buf[1+3+ key_config[i].pos] = '0' + (output_data[key_code] & 1);
      }

    }
   }

      buf[0] = '{';
      buf[1] = 'L';
      buf[2] = 'E';
      buf[3] = 'D';
      buf[68] = '}';
      serial_write(sws,(unsigned char*)buf,69);


}

void lamps_and_displays_update(SESSION_WORK_SPACE *sws)
{
    if(++(sws->disp_cnt) & 1)
      display_update(sws);
    else
      lamps_update(sws);
}


static int on_char(SESSION_WORK_SPACE *sws, SERIAL_PROTO_STRUCT *sps, int ch)
{
           switch(ch)
           {
              case '!': /* ACKed last transmittion*/
                 sws->to = 0;
                 lamps_and_displays_update(sws);
              break;

              case STX:
                sps->cnt = 0;
              break;

              case ETX:
                 sps->len = sps->cnt;
                 return 1;
              break;

              default:
              {
                if(sps->cnt < SERIAL_PROTO_BUF_SIZE)
                {
                  sps->buf[sps->cnt++] = ch;
                }
                else
                  sps->cnt = 0;
              }
            }
return 0;
}

static int listening(SESSION_WORK_SPACE *sws, SERIAL_PROTO_STRUCT *sps)
{
int len;
unsigned char buf[SERIAL_PROTO_BUF_SIZE];
   if((len = serial_read(sws,buf,1)>0))
   {
       if(on_char(sws, sps, buf[0]))
       {
         sps->buf[sps->len] = 0x00;
         return 1;
       }
   }
return 0;
}

DWORD WINAPI serial_thread(LPVOID parms)
{
SESSION_WORK_SPACE	*sws = (SESSION_WORK_SPACE*)parms;
SERIAL_PROTO_STRUCT sps;
memset(&sps,0,sizeof(SERIAL_PROTO_STRUCT));
unsigned long event,ev_type;
sws->live_counter = 0;
sws->to = 15;
  if(sws->serial_port == NULL)
  {
    return 0;
  }
    if(serial_open(sws,sws->serial_port,BAUD_RATE))
    {
      sws->serial_connect_flag = 1;
      set_flag_opened(sws);
    }
    else
    {
      sws->serial_handle = INVALID_HANDLE_VALUE;
      sws->serial_thread_handle = INVALID_HANDLE_VALUE;
      return 0;
    }

      log_write("Thread for serial port %d/%s started",
                sws->port_id,
                sws->serial_port);
      while(is_opened(sws))
      {
         sws->live_counter++;

         if(listening(sws,&sps))
         {
           sps.buf[sps.len] = 0x00;
           event = event2hex(sps.buf);
           ev_type = (event << 4) & 0xF000;
           event &= 0xFF;
           event |= (ev_type | (sws->port_id << 8));
           send_to_sim(event);
         }
         else
         {
            if(++(sws->to) == 20)
            {
              sws->to = 0;
              lamps_and_displays_update(sws);
            }

         }



      }
      log_write("Thread for serial port %s stopped",sws->serial_port);
return 0;
}



