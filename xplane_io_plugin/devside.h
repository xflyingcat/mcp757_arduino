/*--------------------------------------------------------
  "THE BEER-WARE LICENSE" (Revision 42):
  Alex Kostyuk wrote this code. As long as you retain this
  notice you can do whatever you want with this stuff.
  If we meet some day, and you think this stuff is worth it,
  you can buy me a beer in return.
----------------------------------------------------------*/

#ifndef __DEVSIDE_H__
#define __DEVSIDE_H__

enum {
  RISING_EDGE = 0x8000,
  FALLING_EDGE = 0x4000,
  ENCODER_PULSE = 0x2000,
  ROTARY_SWITCH = 0x1000
};


#endif
