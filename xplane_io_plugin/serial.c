/*--------------------------------------------------------
  "THE BEER-WARE LICENSE" (Revision 42):
  Alex Kostyuk wrote this code. As long as you retain this
  notice you can do whatever you want with this stuff.
  If we meet some day, and you think this stuff is worth it,
  you can buy me a beer in return.
----------------------------------------------------------*/
#include "includes.h"


int serial_open(SESSION_WORK_SPACE *sws, const char *dev_name, long baud_rate)
{

DCB dcb;
COMMTIMEOUTS commtimeouts;
char portname[32];

    sprintf(portname, "\\\\.\\%s",dev_name);

	sws->serial_handle = CreateFile(portname,
	                          GENERIC_READ|GENERIC_WRITE,
	                          FILE_SHARE_READ|FILE_SHARE_WRITE,
	                          NULL,
	                          OPEN_EXISTING,
	                          FILE_ATTRIBUTE_NORMAL,
	                          NULL);

	if (sws->serial_handle == INVALID_HANDLE_VALUE)
	{

      return FALSE;
	}

     dcb.DCBlength = sizeof(dcb);

     commtimeouts.ReadIntervalTimeout = 50;
     commtimeouts.ReadTotalTimeoutMultiplier = 0;
     commtimeouts.ReadTotalTimeoutConstant = 100;

     commtimeouts.WriteTotalTimeoutMultiplier = 0;
     commtimeouts.WriteTotalTimeoutConstant = 0;

     	if (GetCommState(sws->serial_handle, &dcb)) {
		dcb.BaudRate = baud_rate;
		dcb.ByteSize = 8;
		dcb.StopBits = ONESTOPBIT;
		dcb.Parity = NOPARITY;
		dcb.fParity = FALSE;
		dcb.fOutxCtsFlow = FALSE;
		dcb.fOutxDsrFlow = FALSE;
		dcb.fDtrControl = FALSE;
		dcb.fRtsControl = FALSE;
		dcb.fDsrSensitivity = FALSE;

		if (SetCommState(sws->serial_handle, &dcb)) {
		    SetCommState(sws->serial_handle, &dcb);

		if (SetCommTimeouts(sws->serial_handle, &commtimeouts))
        {
         	return TRUE;
		}
	}

    }

return FALSE;
}

void serial_close(SESSION_WORK_SPACE *sws)
{
   if(sws->serial_handle != INVALID_HANDLE_VALUE &&
      sws->serial_port[0])
   {

      if(CloseHandle(sws->serial_handle))
          log_write("CloseHandle(sws->serial_handle): %s is successful",sws->serial_port);
      else
          log_write("CloseHandle(sws->serial_handle): %d",GetLastError());

   }
   sws->serial_handle = INVALID_HANDLE_VALUE;
}


int serial_write(SESSION_WORK_SPACE *sws, void *ibuf, int len)
{
DWORD n;
unsigned char *buf = ibuf;
int sentlen = len;

   while(len)
   {
     WriteFile(sws->serial_handle,buf, len, &n, NULL);
     buf += n;
     len -= n;
   }
return sentlen;
}

int serial_read(SESSION_WORK_SPACE *sws, unsigned char *buf,int len)
{
DWORD n;
    ReadFile(sws->serial_handle,
                       buf,
                       len,
                       &n,
                       NULL);
if(n>0)
   return n;
return 0;
}


//static int listening(SESSION_WORK_SPACE *sws, SERIAL_PROTO_STRUCT *sps);
//static int on_char(SERIAL_PROTO_STRUCT *sps, int ch);

void start_serial(SESSION_WORK_SPACE *sws)
{
int i;
load_config("Resources/plugins/");
log_write("USB CDC I/O Hub for X-Plane rev."PLUGIN_REVISION);
   for(i=0;i<SERIAL_PORTS_MAX;i++)
   {
     if(sws[i].serial_port[0] != 0)
     {
       sws[i].serial_connect_flag = 0;
       sws[i].port_id = i;
       sws[i].serial_thread_handle =
             CreateThread(NULL, 0, serial_thread, &sws[i], 0, NULL);
     }
   }
}

void serial_signal_to_close(SESSION_WORK_SPACE *sws)
{
  sws->opened = 0;
}

void set_flag_opened(SESSION_WORK_SPACE *sws)
{
  sws->opened = 1;
}

int is_opened(SESSION_WORK_SPACE *sws)
{
return sws->opened;
}


void serial_stop(SESSION_WORK_SPACE *sws)
{
int i;
  log_write("serial_stop():");

   for(i=0;i<SERIAL_PORTS_MAX;i++)
   {
     if(sws[i].serial_thread_handle != INVALID_HANDLE_VALUE &&
        sws[i].serial_port[0] /* name is not empty */
        )
     {

      serial_signal_to_close(&sws[i]);
      Sleep(100);
#if 0
      if(TerminateThread(sws[i].serial_thread_handle,0))
          log_write("TerminateThread(): %s is successful",sws[i].serial_port);
      else
          log_write("TerminateThread(): %d",GetLastError());
#endif


      if(CloseHandle(sws[i].serial_thread_handle))
          log_write("CloseHandle(sws->serial_thread_handle): %s is successful",sws[i].serial_port);
      else
          log_write("CloseHandle(sws->serial_thread_handle): %d",GetLastError());
     }


     serial_close(&sws[i]);
   }
  log_write("serial_stop(): the serial handles closed");
  log_write("serial_stop(): the serials closed");
}

#define SIMPLE_FIFO_SIZE 5

typedef struct
{
  int   push_indx;
  int   pop_indx;
  unsigned long buff[SIMPLE_FIFO_SIZE];
} SIMPLE_FIFO;

static int pipes_on = 0;
static SIMPLE_FIFO send_to_sim_fifo;
static HANDLE send_to_sim_mutex;

static int fifo_push(SIMPLE_FIFO *fifo, unsigned long data)
{
    int indx;
    indx = fifo->push_indx + 1;
    indx = indx % SIMPLE_FIFO_SIZE;

    if (indx == fifo->pop_indx)
        return 0; // fifo is full
    fifo->buff[fifo->push_indx] = data;
    fifo->push_indx = indx;
    return 1;
}


static int fifo_pop(SIMPLE_FIFO *fifo, unsigned long *data)
{
    int indx;
    if (fifo->push_indx == fifo->pop_indx)
        return 0; // fifo is empty
    indx = fifo->pop_indx + 1;
    indx = indx % SIMPLE_FIFO_SIZE;
    *data = fifo->buff[fifo->pop_indx];
    fifo->pop_indx = indx;
    return 1;
}



// device side

int send_to_sim(unsigned long data)
{

 if(!pipes_on)
       return 0;

 if(WAIT_OBJECT_0 == WaitForSingleObject(send_to_sim_mutex,INFINITE))
 {
     if(fifo_push(&send_to_sim_fifo, data))
     {
       ReleaseMutex(send_to_sim_mutex);
       return 1;
     }
  ReleaseMutex(send_to_sim_mutex);
 }
return 0;
}





// sim side

int recv_from_device(unsigned long *data)
{
 if(!pipes_on)
       return 0;

 if(WAIT_OBJECT_0 == WaitForSingleObject(send_to_sim_mutex,INFINITE))
 {
   if(fifo_pop(&send_to_sim_fifo, data))
   {
     ReleaseMutex(send_to_sim_mutex);
     return 1;
   }
   ReleaseMutex(send_to_sim_mutex);
 }
return 0;
}

void flush_from_device(void)
{
unsigned long data;
 if(WAIT_OBJECT_0 == WaitForSingleObject(send_to_sim_mutex,INFINITE))
 {
   while(fifo_pop(&send_to_sim_fifo, &data));
   ReleaseMutex(send_to_sim_mutex);
 }
}


void pipes_create(void)
{

    memset(&send_to_sim_fifo,0,sizeof(SIMPLE_FIFO));

    send_to_sim_mutex    = CreateMutex(
        NULL,              // default security attributes
        FALSE,             // initially not owned
        NULL);             // unnamed mutex

}

void pipes_destroy(void)
{
unsigned long data;
 if(WAIT_OBJECT_0 == WaitForSingleObject(send_to_sim_mutex,INFINITE))
 {

      pipes_on = 0;
      while(fifo_pop(&send_to_sim_fifo, &data));
      ReleaseMutex(send_to_sim_mutex);


 }

  CloseHandle(send_to_sim_mutex);
  log_write("Mutex semaphore handle closed");
}

void set_pipes_off(void)
{
unsigned long data;

 if(WAIT_OBJECT_0 == WaitForSingleObject(send_to_sim_mutex,INFINITE))
 {
      pipes_on = 0;

      while(fifo_pop(&send_to_sim_fifo, &data));
      ReleaseMutex(send_to_sim_mutex);
 }
}

void set_pipes_on(void)
{
unsigned long data;
 if(WAIT_OBJECT_0 == WaitForSingleObject(send_to_sim_mutex,INFINITE))
 {

      while(fifo_pop(&send_to_sim_fifo, &data));
      pipes_on = 1;
      ReleaseMutex(send_to_sim_mutex);
 }
}


